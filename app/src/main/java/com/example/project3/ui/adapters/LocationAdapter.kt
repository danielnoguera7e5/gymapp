package com.example.project3.ui.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.project3.OnClickListener
import com.example.project3.R
import com.example.project3.databinding.ItemLocationBinding
import com.example.project3.models.Location
import com.example.project3.database.ServiceLocator

class LocationAdapter(private var locationList: List<Location>, private val listener: OnClickListener):
    RecyclerView.Adapter<LocationAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_location, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val location = locationList[position]
        with(holder) {
            setListener(location)
            val pictureBitmap = BitmapFactory.decodeByteArray(location.picture, 0, location.picture!!.size)
            binding.imageImageView.setImageBitmap(pictureBitmap)
            binding.imageImageView.rotation = 90F
            binding.titleTextView.text = location.title
            binding.descriptionTextView.text = location.description

            if(ServiceLocator.isUserOwner(location)) {
                binding.userImageView.visibility = VISIBLE
            } else {
                binding.userImageView.visibility = INVISIBLE }
        }
    }

    override fun getItemCount(): Int {
        return locationList.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = ItemLocationBinding.bind(view)
        fun setListener(location: Location) {
            binding.root.setOnClickListener {
                listener.onClick(location)
            }
        }
    }
}