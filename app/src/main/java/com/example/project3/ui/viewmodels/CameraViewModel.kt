package com.example.project3.ui.viewmodels

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project3.models.Location

class CameraViewModel: ViewModel() {

    val photoTaken: MutableLiveData<Bitmap?> = MutableLiveData<Bitmap?>()

    fun selectPicture(savedUri: String) {
        val thumbImage = ThumbnailUtils.extractThumbnail(
            BitmapFactory.decodeFile(savedUri),
            500, 500
        )
        photoTaken.postValue(thumbImage)
    }

    fun resetPhotoTakenLiveData() {
        if(photoTaken.value != null) { photoTaken.value!!.recycle() }
        photoTaken.postValue(null)
    }
}