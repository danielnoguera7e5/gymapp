package com.example.project3

import com.example.project3.models.Location

interface OnClickListener {
    fun onClick(location: Location)
}