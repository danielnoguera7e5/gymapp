package com.example.project3.database

import com.example.project3.models.Location
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.query.Sort
import io.realm.kotlin.types.ObjectId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocationsDao(val realm: Realm, val userId: String){

    fun listFlowAllLocations() : Flow<List<Location>> =
        realm.query<Location>().sort("title", Sort.DESCENDING).find().asFlow().map { it.list.toList() }

    fun listFlowMyLocations() : Flow<List<Location>> =
        realm.query<Location>("owner_id = '$userId'").sort("title", Sort.DESCENDING).find().asFlow().map { it.list.toList() }

    fun insertLocation(title: String, description: String, latitude: Double,
                       longitude: Double, category: String, picture: ByteArray){
        realm.writeBlocking {
            val location = Location(title = title, description = description, latitude = latitude,
                longitude = longitude, category = category, picture = picture, owner_id = userId)
            copyToRealm(location)
        }
    }
    fun deleteLocation(_id: ObjectId) {
        realm.writeBlocking {
            val location = realm.query<Location>("_id == $0", _id).find().first()
            findLatest(location)?.let { delete(it) }
        }
    }
}