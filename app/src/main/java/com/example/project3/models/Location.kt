package com.example.project3.models

import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey


open class Location(
    @PrimaryKey
    var _id: ObjectId = ObjectId.create(),
    var title: String = "",
    var description: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var category: String = "",
    var picture: ByteArray? = null,
    var owner_id: String = ""
) : RealmObject {
    constructor() : this(owner_id = "") {}
}