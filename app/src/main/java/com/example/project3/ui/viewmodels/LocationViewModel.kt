package com.example.project3.ui.viewmodels

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project3.models.Location
import com.example.project3.database.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

class LocationViewModel: ViewModel() {

    val locationList = MutableLiveData<MutableList<Location>>(mutableListOf())
    val provisionalLocation = MutableLiveData<Location?>()
    val selectedLocation = MutableLiveData<Location>()

    init {
        obtainAllLocations()
    }

    fun obtainAllLocations() {
        CoroutineScope(Dispatchers.IO).launch {
            val locationFlow = ServiceLocator.locationsDao.listFlowAllLocations()
            locationFlow.collect { loc -> locationList.postValue(loc.toMutableList()) }
        }
    }

    fun obtainMyLocations() {
        CoroutineScope(Dispatchers.IO).launch {
            val locationFlow = ServiceLocator.locationsDao.listFlowMyLocations()
            locationFlow.collect { loc -> locationList.postValue(loc.toMutableList()) }
        }
    }

    fun addProvisionalLocation(location: Location) {
        provisionalLocation.postValue(location)
    }

    fun addLocation(location: Location?, photoTaken: Bitmap?) {
        location!!.picture = bitmapToByteArray(photoTaken!!)
        CoroutineScope(Dispatchers.IO).launch {
            location.apply {
                ServiceLocator.locationsDao.insertLocation(
                    title, description, latitude, longitude, category, picture!!)
            }
        }
    }

    fun select(location: Location) {
        selectedLocation.postValue(location)
    }

    fun bitmapToByteArray(photoTaken: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        photoTaken.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }
}