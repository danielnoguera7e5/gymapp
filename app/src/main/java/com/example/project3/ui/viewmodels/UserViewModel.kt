package com.example.project3.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project3.database.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {
    val loggedIn = MutableLiveData<Boolean>()

    fun startSplash() {
        if(ServiceLocator.realmManager.loggedIn()){
            CoroutineScope(Dispatchers.IO).launch {
                ServiceLocator.realmManager.configureRealm()
                loggedIn.postValue(true)
            }
        } else {
            loggedIn.postValue(false)
        }
    }
}