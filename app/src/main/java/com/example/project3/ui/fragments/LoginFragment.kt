package com.example.project3.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation.findNavController
import com.example.project3.R
import com.example.project3.databinding.FragmentLoginBinding
import com.example.project3.database.ServiceLocator
import com.example.project3.ui.viewmodels.UserViewModel
import kotlinx.coroutines.*


class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding
    val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        CoroutineScope(Dispatchers.IO).launch {
            ServiceLocator.realmManager.logout()
            userViewModel.loggedIn.postValue(false)
        }

        binding.loginButton.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                kotlin.runCatching {
                    ServiceLocator.realmManager.login(
                        binding.inputUser.text.toString(),
                        binding.inputPassword.text.toString()
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        findNavController(binding.root).navigate(R.id.action_loginFragment_to_mapFragment)
                        userViewModel.loggedIn.postValue(true)
                    }
                }.onFailure {
                    withContext(Dispatchers.Main) {
                        Toast.makeText(context, "INVALID USERNAME/PASSWORD", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }

        binding.registerButton.setOnClickListener {
            findNavController(binding.root).navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }
}