package com.example.project3.database

import com.example.project3.models.Location
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.App
import io.realm.kotlin.mongodb.AppConfiguration
import io.realm.kotlin.mongodb.Credentials
import io.realm.kotlin.mongodb.subscriptions
import io.realm.kotlin.mongodb.sync.SyncConfiguration
import kotlinx.coroutines.runBlocking


class RealmManager {
    val realmApp = App.create(AppConfiguration.Builder("gymapp-ocdzu").log(LogLevel.ALL).build())
    var realm : Realm? = null

    fun loggedIn() = realmApp.currentUser?.loggedIn ?: false

    suspend fun register(username: String, password: String) {
        realmApp.emailPasswordAuth.registerUser(username, password)
        login(username, password)
    }
    suspend fun login(username: String, password: String) {
        val creds = Credentials.emailPassword(username, password)
        realmApp.login(creds)
        configureRealm()
    }

    suspend fun logout() { realmApp.currentUser?.logOut() }

    fun configureRealm() {
        requireNotNull(realmApp.currentUser)
        runBlocking {
            val user = realmApp.currentUser!!
            val config = SyncConfiguration.Builder(user, setOf(Location::class))
                .initialSubscriptions { realm ->
                    add(
                        realm.query<Location>(),
                        "All Items"
                    )
                }
                .waitForInitialRemoteData()
                .build()
            realm = Realm.open(config)
            realm!!.subscriptions.waitForSynchronization()

            ServiceLocator.configureRealm()
        }
    }
}