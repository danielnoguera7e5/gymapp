package com.example.project3.database

import com.example.project3.models.Location


object ServiceLocator {
    val realmManager = RealmManager()
    lateinit var locationsDao: LocationsDao

    fun configureRealm(){
        requireNotNull(realmManager.realm)
        val realm = realmManager.realm!!
        locationsDao = LocationsDao(realm, realmManager.realmApp.currentUser!!.id)
    }

    fun isUserOwner(location: Location): Boolean {
        return location.owner_id == realmManager.realmApp.currentUser!!.id
    }
}