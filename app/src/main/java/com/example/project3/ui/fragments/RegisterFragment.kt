package com.example.project3.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation.findNavController
import com.example.project3.R
import com.example.project3.databinding.FragmentRegisterBinding
import com.example.project3.database.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class RegisterFragment : Fragment() {
    // val model?
    private lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.registerButton.setOnClickListener {

            CoroutineScope(Dispatchers.IO).launch {
                kotlin.runCatching {
                    ServiceLocator.realmManager.register(
                        binding.inputUser.text.toString(),
                        binding.inputPassword.text.toString()
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        findNavController(binding.root).navigate(R.id.action_registerFragment_to_mapFragment)
                    }
                }.onFailure {
                    withContext(Dispatchers.Main) {
                        Toast.makeText(context, "INVALID USERNAME/PASSWORD", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}