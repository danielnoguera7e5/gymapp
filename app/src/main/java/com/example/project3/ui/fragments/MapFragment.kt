package com.example.project3.ui.fragments

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.project3.R
import com.example.project3.databinding.FragmentMapBinding
import com.example.project3.ui.viewmodels.CameraViewModel
import com.example.project3.models.Location
import com.example.project3.ui.viewmodels.LocationViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


const val REQUEST_CODE_LOCATION = 100

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMapLongClickListener {
    lateinit var binding: FragmentMapBinding
    lateinit var map: GoogleMap
    private val cameraModel: CameraViewModel by activityViewModels()
    private val locationModel: LocationViewModel by activityViewModels()
    var existingMarker = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMapBinding.inflate(layoutInflater)
        createMap()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        binding.newLocationButton.isEnabled = false

        binding.newLocationButton.setOnClickListener {
            cameraModel.resetPhotoTakenLiveData()
            Navigation.findNavController(binding.root).navigate(R.id.action_mapFragment_to_addLocationFragment)
        }

        binding.logoutButton.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.loginFragment)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnMapLongClickListener(this)
        enableLocation()
        zoomCurrentLocation()
    }

    fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    fun createMarker(latitude: Double, longitude: Double) {
        val coordinates = LatLng(latitude, longitude)
        val myMarker = MarkerOptions().position(coordinates)
        map.addMarker(myMarker)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
            1000, null)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }

    fun zoomCurrentLocation() {
        val locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val criteria = Criteria()

        val location = locationManager!!.getLastKnownLocation(
            locationManager!!.getBestProvider(
                criteria,
                false
            )!!
        )
        if (location != null) {
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        location.latitude,
                        location.longitude
                    ), 13f
                )
            )
            val cameraPosition = CameraPosition.Builder()
                .target(
                    LatLng(
                        location.latitude,
                        location.longitude
                    )
                ).zoom(17f).build()
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }

    override fun onMapLongClick(latLng: LatLng) {
        binding.newLocationButton.isEnabled = true
        if(!existingMarker) {
            createMarker(latLng.latitude, latLng.longitude)
            existingMarker = true
        } else {
            map.clear()
            createMarker(latLng.latitude, latLng.longitude)
        }
        locationModel.addProvisionalLocation(Location(
            latitude = latLng.latitude,
            longitude = latLng.longitude
        ))
    }

}