package com.example.project3.ui.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.project3.ui.viewmodels.LocationViewModel
import com.example.project3.R
import com.example.project3.databinding.FragmentDetailBinding
import com.example.project3.database.ServiceLocator


class DetailFragment : Fragment() {

    private val locationModel: LocationViewModel by activityViewModels()
    lateinit var binding: FragmentDetailBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        locationModel.selectedLocation.observe(viewLifecycleOwner, Observer {
            val location = it
            val pictureBitmap = BitmapFactory.decodeByteArray(location.picture, 0, location.picture!!.size)
            binding.locationImageView.setImageBitmap(pictureBitmap)
            binding.locationImageView.rotation = 90F
            binding.locationTitleView.text = location.title
            binding.locationDescriptionView.text = location.description
            binding.locationCategoryView.text = location.category
            binding.locationLatitudeView.text = location.latitude.toString()
            binding.locationLongitudeView.text = location.longitude.toString()
            binding.deleteButton.isEnabled = ServiceLocator.isUserOwner(location)
        })

        binding.deleteButton.setOnClickListener {
            ServiceLocator.locationsDao.deleteLocation(locationModel.selectedLocation.value!!._id)
            Navigation.findNavController(binding.root).navigate(R.id.recyclerFragment)
        }
    }
}