package com.example.project3.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.project3.ui.adapters.LocationAdapter
import com.example.project3.ui.viewmodels.LocationViewModel
import com.example.project3.OnClickListener
import com.example.project3.R
import com.example.project3.databinding.FragmentRecyclerBinding
import com.example.project3.models.Location
import com.example.project3.database.ServiceLocator


class RecyclerFragment : Fragment(), OnClickListener {

    private lateinit var locationAdapter: LocationAdapter
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerBinding
    private val locationModel: LocationViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView(locationModel.locationList.value!!)

        locationModel.locationList.observe(viewLifecycleOwner, Observer {
            if (locationModel.locationList.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(locationModel.locationList.value!!)
                locationAdapter.notifyDataSetChanged()
            }
        })

        binding.myLocationsButton.setOnClickListener {
            locationModel.obtainMyLocations()
        }

        binding.allLocationsButton.setOnClickListener {
            locationModel.obtainAllLocations()
        }
    }

    private fun setUpRecyclerView(locationList: MutableList<Location>) {
        gridLayoutManager = GridLayoutManager(context, 1)
        if (locationList != null) {
            locationAdapter = LocationAdapter(locationList, this)
            binding.recyclerView.apply {
                setHasFixedSize(true)
                layoutManager = gridLayoutManager
                adapter = locationAdapter
            }
        }
    }

    override fun onClick(location: Location) {
        locationModel.select(location)
        ServiceLocator.configureRealm()
        Navigation.findNavController(binding.root).navigate(R.id.action_recyclerFragment_to_detailFragment)
    }

}