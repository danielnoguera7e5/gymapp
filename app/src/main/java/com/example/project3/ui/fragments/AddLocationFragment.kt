package com.example.project3.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.project3.ui.viewmodels.LocationViewModel
import com.example.project3.R
import com.example.project3.databinding.FragmentAddLocationBinding
import com.example.project3.ui.viewmodels.CameraViewModel
import com.example.project3.models.Location


class AddLocationFragment : Fragment() {

    lateinit var binding: FragmentAddLocationBinding
    private val cameraModel: CameraViewModel by activityViewModels()
    private val locationModel: LocationViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentAddLocationBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val categoryItems = listOf("Private", "Public", "Other")
        val adapter = ArrayAdapter(requireContext(), R.layout.category_item, categoryItems)
        binding.locationCategoryTextView.setAdapter(adapter)

        binding.cameraPictureImage.rotation = 90F

        cameraModel.photoTaken.observe(viewLifecycleOwner, Observer {
            binding.cameraPictureImage.setImageBitmap(it)
            checkAllLocationFields()
        })

        locationModel.provisionalLocation.observe(viewLifecycleOwner, Observer {
            binding.locationTitleField.setText(it?.title ?: "")
            binding.locationDescriptionField.setText(it?.description ?: "")
            binding.latitudeValueTextView.text = it?.latitude.toString()
            binding.longitudeValueTextView.text = it?.longitude.toString()
            binding.locationCategoryTextView.setText(it?.category ?: "")
        })

        binding.cameraCaptureButton.setOnClickListener {
            locationModel.addProvisionalLocation(
                Location(
                title = binding.locationTitleField.text.toString(),
                description = binding.locationDescriptionField.text.toString(),
                latitude = binding.latitudeValueTextView.text.toString().toDouble(),
                longitude = binding.longitudeValueTextView.text.toString().toDouble(),
                category = binding.locationCategoryTextView.text.toString(),
                )
            )
            Navigation.findNavController(binding.root).navigate(R.id.action_addLocationFragment_to_cameraFragment)
        }

        binding.locationTitleField.addTextChangedListener(watcher)
        binding.locationDescriptionField.addTextChangedListener(watcher)
        binding.latitudeValueTextView.addTextChangedListener(watcher)
        binding.longitudeValueTextView.addTextChangedListener(watcher)
        binding.locationCategoryTextView.addTextChangedListener(watcher)

        binding.newLocationButton.setOnClickListener {
            locationModel.addLocation(locationModel.provisionalLocation.value!!, cameraModel.photoTaken.value!!)
            Navigation.findNavController(binding.root).navigate(R.id.mapFragment)
        }
    }

    fun checkAllLocationFields() {
        binding.newLocationButton.isEnabled = !(binding.locationTitleField.text.toString() == "" ||
                binding.locationDescriptionField.text.toString() == "" ||
                binding.latitudeValueTextView.text.toString() == "" ||
                binding.longitudeValueTextView.text.toString() == "" ||
                binding.locationCategoryTextView.text.toString() == "" ||
                cameraModel.photoTaken.value == null)
    }

    private val watcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(s: Editable) {
            checkAllLocationFields()
        }
    }
}