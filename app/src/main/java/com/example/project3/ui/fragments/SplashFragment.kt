package com.example.project3.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.project3.R
import com.example.project3.ui.viewmodels.UserViewModel


class SplashFragment : Fragment() {

    companion object { fun newInstance() = SplashFragment() }

    private lateinit var viewModel: UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        viewModel.loggedIn.observe(viewLifecycleOwner){loggedIn ->
            if(loggedIn){
                findNavController().navigate(R.id.action_splashFragment_to_mapFragment)
            } else {
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            }
        }
        viewModel.startSplash()
    }
}